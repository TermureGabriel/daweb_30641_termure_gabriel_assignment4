<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'doctor_id',
        'date',
    ];

}
