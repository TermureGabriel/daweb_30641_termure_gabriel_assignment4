<?php


namespace App\Http\Controllers;
use App\Models\Appointment;
use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class DoctorController extends Controller
{
    public function findDoctorById($id) {
        $doctor = array();
        if($id != "") {
            $doctor = Doctor::find($id);
            return $doctor;
        }
    }

    public function findAllDoctors(){
        $allDoctors = Doctor::all();
        if(count($allDoctors) > 0) {
            return response()->json(["status" => "success", "success" => true, "count" => count($allDoctors), "data" => $allDoctors]);
        }
        else {
            return response()->json(["status" => "failed", "success" => false, "message" => "Whoops! no record found"]);
        }
    }

    public function findDoctorAppointments($email){
        $appointments = array();
        if($email != "") {
            $appointments = DB::table('appointments')
                ->join('doctors', 'appointments.doctor_id', '=', 'doctors.id')
                ->join('users', 'appointments.user_id', '=', 'users.id')
                ->select('users.name', 'appointments.date')
                ->where('doctors.email', $email)
                ->get();

            return $appointments;
        }
    }
}
