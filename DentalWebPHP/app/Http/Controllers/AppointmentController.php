<?php


namespace App\Http\Controllers;
use App\Models\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class AppointmentController extends Controller
{
    public function addAppointment(Request $request){
        $appointment = new Appointment();
        $appointment->user_id = $request->user_id;
        $appointment->doctor_id = $request->doctor_id;
        $appointment->date = $request->date;

        $appointment->save();
        return $appointment;
    }

    public function getFrequencyAppointmentFromLastMonth($email){
        $appointments = array();
        $appointments = DB::table('appointments')
                        ->join('doctors', 'appointments.doctor_id', '=', 'doctors.id')
                        ->select((DB::raw('DATE(date) as date')), DB::raw('COUNT(*) AS appointments'))
                        ->where('doctors.email', $email)
                        ->where('date','>=',Carbon::now()->subdays(30))
                        ->groupBy(DB::raw('DATE(date)'))
                        ->get();
        return $appointments;
    }


}
