<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\AppointmentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();
});

Route::post('user-login', [UserController::class, 'userLogin']);
Route::get('user/{email}', [UserController::class, 'userDetail']);
Route::put('user/update', [UserController::class, 'updateUser']);
Route::get('user/findById/{id}', [UserController::class, 'findUserById']);

Route::get('doctor/findById/{id}', [DoctorController::class, 'findDoctorById']);
Route::post('appointment/add', [AppointmentController::class, 'addAppointment']);
Route::get('doctor/getAllDoctors', [DoctorController::class, 'findAllDoctors']);
Route::get('doctor/getDoctorAppointments/{email}', [DoctorController::class, 'findDoctorAppointments']);

Route::get('appointment/getAppointmentsStatistics/{email}', [AppointmentController::class, 'getFrequencyAppointmentFromLastMonth']);

