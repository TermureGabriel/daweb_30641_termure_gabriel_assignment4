import React,{Component} from 'react';
import {useTranslation} from "react-i18next";
import i18n from "./i18n";

class Select extends Component{


    change(option){

        //localStrage
        localStorage.setItem('lang', option.target.value)
        //cookie
        let today = new Date();
        let expire = new Date();
        let nDays = 1;


        expire.setTime(today.getTime() + 3600000*24*nDays);
        document.cookie = 'lang'+"="+ escape(option.target.value) + ";expires="+expire.toString();
        i18n.changeLanguage(option.target.value);

        //window.location.reload();
    }


     getCookie(c_name) {
        if (document.cookie.length > 0) {
           let c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
               let c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) {
                    c_end = document.cookie.length;
                }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }

    render(){
        return(
         <div>
             <select onChange={this.change} value={this.getCookie('lang')}>
                 <option value={"en"}>English</option>
                 <option value={"ro"}>Romana</option>
             </select>
         </div>
        )
    }
}
export default Select;