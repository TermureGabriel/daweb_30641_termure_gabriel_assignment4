import {Nav, Navbar} from "react-bootstrap";
import Logo from "./images/dental-logo-8.png";
import {LinkContainer} from "react-router-bootstrap";
import tooth from "./images/tooth-logo1.png";
import "./style/Contact.css"
import phone from "./images/icon-phone.png";
import mail from "./images/icon-mail.png";
import location from "./images/icon-location.png";
import {useTranslation} from "react-i18next";
import MailComponent from './mail-component.js'
import axios from "axios";


function handleClick(e) {
    e.preventDefault();
    console.log(document.getElementById("Message").value);
    console.log(document.getElementById("Email").value);
    // document.getElementById("Name").value;



    axios.post('http://127.0.0.1:8000/mail',{},{params:{
            name: document.getElementById("Name").value,
            email: document.getElementById("Email").value,
            message: document.getElementById("Message").value
        }

    })
        .then((response) => {
            console.log(response);
        }, (error) => {
            console.log(error);
        });
}




function Contact(){
    const {t}= useTranslation();
    return(
        <div className="Contact">
            <Navbar className="nav-bar-universal" variant="dark">
                <Navbar.Brand href="#home">
                    <img className="d-inline-block align-top poz"
                         alt="logo"
                         src={Logo}
                         width="90"
                         height="90"
                    />
                </Navbar.Brand>

                <Navbar.Brand href="#home" className="logo-txt">
                    <p>
                        <h3 id="txt-1-unv">Dental </h3>
                        <h3 id="txt-2">Clinic</h3>
                    </p>
                </Navbar.Brand>


                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto poz">
                        <LinkContainer to={"/"}>
                            <Nav.Link id="text-style" >{t('menu.acasa')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/noutati"}>
                            <Nav.Link id="text-style">{t('menu.noutati')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/despre-noi"}>
                            <Nav.Link id="text-style">{t('menu.despreNoi')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/medici"}>
                            <Nav.Link id="text-style">{t('menu.medici')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/servicii-tarife"}>
                            <Nav.Link id="text-style">{t('menu.servicii')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/contact"}>
                            <Nav.Link id="text-style">{t('menu.contact')}</Nav.Link>
                        </LinkContainer>
                    </Nav>

                </Navbar.Collapse>

            </Navbar>

            <div className="decor-div">
                <h1>{t("contact1")}</h1>
            </div>

            <div className="contact-zone">
                <img
                    alt="Img-tooth"
                    src={tooth}
                    width="50"
                    height="50"
                />

                <h2>{t("contact2")}</h2>


                <section>

                    <div className="row">

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src={phone}
                                 className="rounded-circle" alt="doctor_img"
                                 width={"100"}
                                 height={"100"}/>

                            <h5>{t("contact.telephone")}</h5>
                            <h6>+40 754 852.736</h6>

                        </div>

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src={mail}
                                 className="rounded-circle" alt="doctor_img"
                                 width={"100"}
                                 height={"100"}/>

                            <h5>Email</h5>
                            <h6>dentalclinic@gmail.com</h6>

                        </div>

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src={location}
                                 className="rounded-circle" alt="doctor_img"
                                 width={"100"}
                                 height={"100"}/>

                            <h5>{t("contact.location")}</h5>
                            <h6>Str. Avram Iancu nr 49, Cluj-Napoca, Cluj, Romania</h6>

                        </div>

                    </div>


                </section>


                <div>
                    <iframe width="100%" height="300"
                                src="https://maps.google.com/maps?q=Avram%20iancu%20cluj&t=&z=13&ie=UTF8&iwloc=&output=embed">
                    </iframe>
                </div>


            </div>

            <br></br>
            <br></br>
            <br></br>

            <div className={"contact-zone"}>
                <img
                    alt="Img-tooth"
                    src={tooth}
                    width="50"
                    height="50"
                />

                <h2>{t("contact.mail.title")}</h2>
            </div>

            <div className="fcf-body">


                <div id="fcf-form">


                    <form method="post" action="contact-form-process.php">

                        <div className="fcf-form-group">
                            <label className="fcf-label">{t("contact.mail.name")}</label>
                            <div>
                                <input type="text" id="Name" name="Name" className="fcf-form-control" />
                            </div>
                        </div>

                        <div className="fcf-form-group">
                            <label className="fcf-label">{t("contact.mail.email")}</label>
                            <div>
                                <input type="email" id="Email" name="Email" className="fcf-form-control" />
                            </div>
                        </div>

                        <div className="fcf-form-group">
                            <label className="fcf-label">{t("contact.mail.message")}</label>
                            <div>
                                <textarea id="Message" name="Message" className="fcf-form-control" rows="6"
                                          maxLength="3000" required> </textarea>
                            </div>
                        </div>

                        <div className="fcf-form-group">
                            <button type="submit" id="fcf-button" onClick={handleClick}
                                    className="fcf-btn fcf-btn-primary fcf-btn-lg fcf-btn-block">{t("contact.mail.button")}
                            </button>
                        </div>

                    </form>
                </div>

            </div>


        </div>
    );
}

export default Contact;