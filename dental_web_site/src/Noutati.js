import {Nav, Navbar} from "react-bootstrap";
import Logo from "./images/dental-logo-8.png";
import {LinkContainer} from "react-router-bootstrap";
import {Card, CardColumns} from "react-bootstrap";
import "./style/Noutati.css";
import tooth from "./images/tooth-logo1.png";
import news1 from "./images/news-1.jpg";
import news2 from "./images/news-2.jpg";
import news3 from "./images/news-3.jpg";
import news4 from "./images/news-5.jpg";
import xml from "./xml/news.xml"

import {useTranslation} from "react-i18next";



function Noutati(){
    const {t}= useTranslation();

    fetch(xml).then(function(resp){
        return resp.text();
    }).then(function(data){
        let parser = new DOMParser(),
            xmlDoc = parser.parseFromString(data, 'text/xml');


        if(localStorage.getItem('lang')==='ro'){
            document.getElementById('page-title').innerHTML = xmlDoc.getElementById('page-title-ro').innerHTML;

            document.getElementById('card1-title').innerHTML = xmlDoc.getElementById('card1-title-ro').innerHTML;
            document.getElementById('card1-description').innerHTML = xmlDoc.getElementById('card1-text-ro').innerHTML;
            document.getElementById('card1-footer').innerHTML = xmlDoc.getElementById('card1-footer-ro').innerHTML;

            document.getElementById('card2-title').innerHTML = xmlDoc.getElementById('card2-title-ro').innerHTML;
            document.getElementById('card2-description').innerHTML = xmlDoc.getElementById('card2-text-ro').innerHTML;
            document.getElementById('card2-footer').innerHTML = xmlDoc.getElementById('card2-footer-ro').innerHTML;

            document.getElementById('card3-description').innerHTML = xmlDoc.getElementById('card3-text-ro').innerHTML;
            document.getElementById('card3-footer').innerHTML = xmlDoc.getElementById('card3-footer-ro').innerHTML;

            document.getElementById('card4-title').innerHTML = xmlDoc.getElementById('card4-title-ro').innerHTML;
            document.getElementById('card4-description').innerHTML = xmlDoc.getElementById('card4-text-ro').innerHTML;
            document.getElementById('card4-footer').innerHTML = xmlDoc.getElementById('card4-footer-ro').innerHTML;

            document.getElementById('card5-description').innerHTML = xmlDoc.getElementById('card5-text-ro').innerHTML;
            document.getElementById('card5-footer').innerHTML = xmlDoc.getElementById('card5-footer-ro').innerHTML;

            document.getElementById('card6-title').innerHTML = xmlDoc.getElementById('card6-title-ro').innerHTML;
            document.getElementById('card6-description').innerHTML = xmlDoc.getElementById('card6-text-ro').innerHTML;
            document.getElementById('card6-footer').innerHTML = xmlDoc.getElementById('card6-footer-ro').innerHTML;


        }else if(localStorage.getItem('lang')==='en'){
            document.getElementById('page-title').innerHTML = xmlDoc.getElementById('page-title-en').innerHTML;

            document.getElementById('card1-title').innerHTML = xmlDoc.getElementById('card1-title-en').innerHTML;
            document.getElementById('card1-description').innerHTML = xmlDoc.getElementById('card1-text-en').innerHTML;
            document.getElementById('card1-footer').innerHTML = xmlDoc.getElementById('card1-footer-en').innerHTML;

            document.getElementById('card2-title').innerHTML = xmlDoc.getElementById('card2-title-en').innerHTML;
            document.getElementById('card2-description').innerHTML = xmlDoc.getElementById('card2-text-en').innerHTML;
            document.getElementById('card2-footer').innerHTML = xmlDoc.getElementById('card2-footer-en').innerHTML;

            document.getElementById('card3-description').innerHTML = xmlDoc.getElementById('card3-text-en').innerHTML;
            document.getElementById('card3-footer').innerHTML = xmlDoc.getElementById('card3-footer-en').innerHTML;

            document.getElementById('card4-title').innerHTML = xmlDoc.getElementById('card4-title-en').innerHTML;
            document.getElementById('card4-description').innerHTML = xmlDoc.getElementById('card4-text-en').innerHTML;
            document.getElementById('card4-footer').innerHTML = xmlDoc.getElementById('card4-footer-en').innerHTML;

            document.getElementById('card5-description').innerHTML = xmlDoc.getElementById('card5-text-en').innerHTML;
            document.getElementById('card5-footer').innerHTML = xmlDoc.getElementById('card5-footer-en').innerHTML;

            document.getElementById('card6-title').innerHTML = xmlDoc.getElementById('card6-title-en').innerHTML;
            document.getElementById('card6-description').innerHTML = xmlDoc.getElementById('card6-text-en').innerHTML;
            document.getElementById('card6-footer').innerHTML = xmlDoc.getElementById('card6-footer-en').innerHTML;

        }

    });


    return(
        <div className="App">
            <Navbar className="nav-bar-universal" variant="dark">
                <Navbar.Brand href="#home">
                    <img className="d-inline-block align-top poz"
                         alt="logo"
                         src={Logo}
                         width="90"
                         height="90"
                    />
                </Navbar.Brand>

                <Navbar.Brand href="#home" className="logo-txt">
                    <p>
                        <h3 id="txt-1-unv">Dental </h3>
                        <h3 id="txt-2">Clinic</h3>
                    </p>
                </Navbar.Brand>


                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto poz">
                        <LinkContainer to={"/"}>
                            <Nav.Link id="text-style" >{t('menu.acasa')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/noutati"}>
                            <Nav.Link id="text-style">{t('menu.noutati')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/despre-noi"}>
                            <Nav.Link id="text-style">{t('menu.despreNoi')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/medici"}>
                            <Nav.Link id="text-style">{t('menu.medici')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/servicii-tarife"}>
                            <Nav.Link id="text-style">{t('menu.servicii')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/contact"}>
                            <Nav.Link id="text-style">{t('menu.contact')}</Nav.Link>
                        </LinkContainer>
                    </Nav>

                </Navbar.Collapse>

            </Navbar>

            <div className="decor-div">
                <h1 id={'page-title'}> </h1>
            </div>

            <div className="news-zone">
                <CardColumns>
                    <Card>
                        <Card.Img variant="top" src={news1} />
                        <Card.Body>
                            <Card.Title id={'card1-title'}></Card.Title>
                            <Card.Text id={'card1-description'}>
                            </Card.Text>
                        </Card.Body>
                        <Card.Footer>
                            <small id={'card1-footer'} className="text-muted"> </small>
                        </Card.Footer>
                    </Card>


                    <Card>
                        <Card.Img variant="top" src={news2} />
                        <Card.Body>
                            <Card.Title id={'card2-title'}> </Card.Title>
                            <Card.Text id={'card2-description'}>
                            </Card.Text>
                        </Card.Body>
                        <Card.Footer>
                            <small id={'card2-footer'} className="text-muted"> </small>
                        </Card.Footer>
                    </Card>

                    <Card bg="primary" text="white" className="text-center p-3">
                        <blockquote className="blockquote mb-0 card-body">
                            <p id={'card3-description'}>
                            </p>
                            <footer className="blockquote-footer">
                                <small id={'card3-footer'} className="text-muted"> </small>
                            </footer>
                        </blockquote>
                    </Card>

                    <Card className="text-center">
                        <Card.Body>
                            <Card.Title id={'card4-title'}> </Card.Title>
                            <Card.Text id={'card4-description'}>
                            </Card.Text>
                            <Card.Text>
                                <small id={'card4-footer'} className="text-muted"> </small>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img src={news3} />
                    </Card>
                    <Card className="text-right">
                        <blockquote className="blockquote mb-0 card-body">
                            <p id={'card5-description'}>
                            </p>
                            <footer className="blockquote-footer">
                                <small id={'card5-footer'} className="text-muted"> </small>
                            </footer>
                        </blockquote>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={news4} />
                        <Card.Body>
                            <Card.Title id={'card6-title'}> </Card.Title>
                            <Card.Text id={'card6-description'}>
                            </Card.Text>
                        </Card.Body>
                        <Card.Footer>
                            <small id={'card6-footer'} className="text-muted"> </small>
                        </Card.Footer>
                    </Card>


                </CardColumns>
            </div>

        </div>


    );



}

export default Noutati;