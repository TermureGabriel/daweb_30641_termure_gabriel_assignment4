import i18n from 'i18next';
import { initReactI18next} from 'react-i18next';
import  SelectLanguage from'./select-component';

import translationEN from './traducere/en/translationEN.json';
import translationRO from './traducere/ro/translationRO.json';


const resources = {
    en:{
        translation: translationEN
    },
    ro: {
        translation: translationRO
    }
};



i18n
    .use(initReactI18next)
    .init({
        resources,
        lng: localStorage.getItem('lang'),
        keySeparator: false,
        interpolation: {
            escapeValue: false
        }
    });

export default i18n;