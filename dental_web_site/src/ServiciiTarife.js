import {Nav, Navbar} from "react-bootstrap";
import Logo from "./images/dental-logo-8.png";
import {LinkContainer} from "react-router-bootstrap";
import "./style/Servicii.css"
import tooth from "./images/tooth-logo1.png";
import implant2 from "./images/implant-dentar-2.jpg"
import aparat from "./images/aparat-dentar.jpg";
import stomatologieCopii from "./images/stomatologie-copii.jpg";
import chirurgie from "./images/chirurgie-orala.jpg";
import {useTranslation} from "react-i18next";


function ServiciiTarife(){
    const {t}= useTranslation();
    return(
        <div className="Servicii">
            <Navbar className="nav-bar-universal" variant="dark">
                <Navbar.Brand href="#home">
                    <img className="d-inline-block align-top poz"
                         alt="logo"
                         src={Logo}
                         width="90"
                         height="90"
                    />
                </Navbar.Brand>

                <Navbar.Brand href="#home" className="logo-txt">
                    <p>
                        <h3 id="txt-1-unv">Dental </h3>
                        <h3 id="txt-2">Clinic</h3>
                    </p>
                </Navbar.Brand>


                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto poz">
                        <LinkContainer to={"/"}>
                            <Nav.Link id="text-style" >{t('menu.acasa')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/noutati"}>
                            <Nav.Link id="text-style">{t('menu.noutati')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/despre-noi"}>
                            <Nav.Link id="text-style">{t('menu.despreNoi')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/medici"}>
                            <Nav.Link id="text-style">{t('menu.medici')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/servicii-tarife"}>
                            <Nav.Link id="text-style">{t('menu.servicii')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/contact"}>
                            <Nav.Link id="text-style">{t('menu.contact')}</Nav.Link>
                        </LinkContainer>
                    </Nav>

                </Navbar.Collapse>

            </Navbar>

            <div className="decor-div">
                <h1>{t("services.title")}</h1>
            </div>

            <div className="servicii-zone">
                <img
                    alt="Img-tooth"
                    src={tooth}
                    width="50"
                    height="50"
                />
                <br></br>
                <h3>{t("services1")}</h3>

            </div>


            <div className="tarif">
                <div className="row">
                    <div className="column">
                        <img
                            alt="implant"
                            src={implant2}
                            width="300"
                            height="200"
                        />
                    </div>

                    <div className="column">
                        <p>{t("services1.description")}</p>



                        <table>
                            <thead>
                            <tr>
                                <th scope="col">{t("services.column1")}</th>
                                <th scope="col">{t("services.column2")}</th>
                                <th scope="col">{t("services.column3")}</th>
                                <th scope="col">{t("services.column4")}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td data-label="Manopera">{t("services1.subtitle1")}</td>
                                <td data-label="Pret initial">2150</td>
                                <td data-label="Reducere card">10%</td>
                                <td data-label="Pret final">1935</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services1.subtitle2")}</td>
                                <td data-label="Pret initial"> 2500</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">2250</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services1.subtitle3")}</td>
                                <td data-label="Pret initial">1800</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">1620</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services1.subtitle4")}</td>
                                <td data-label="Pret initial">2400</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">2160</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services1.subtitle5")}</td>
                                <td data-label="Pret initial">640</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">576</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>


            <div>
                <img
                    alt="Img-tooth"
                    src={tooth}
                    width="50"
                    height="50"
                />
                <br></br>
                <h3>{t("services2")}</h3>

            </div>


            <div className="tarif">
                <div className="row">
                    <div className="column">
                        <img
                            alt="Aparat"
                            src={aparat}
                            width="300"
                            height="200"
                        />
                    </div>

                    <div className="column">
                        <p>{t("services2.description")}</p>



                        <table>
                            <thead>
                            <tr>
                                <th scope="col">{t("services.column1")}</th>
                                <th scope="col">{t("services.column2")}</th>
                                <th scope="col">{t("services.column3")}</th>
                                <th scope="col">{t("services.column4")}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td data-label="Manopera">{t("services2.subtitle1")}</td>
                                <td data-label="Pret initial">1600</td>
                                <td data-label="Reducere card">10%</td>
                                <td data-label="Pret final">1440</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services2.subtitle2")}</td>
                                <td data-label="Pret initial">1700</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">1530</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services2.subtitle3")}</td>
                                <td data-label="Pret initial">2900</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">2610</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services2.subtitle4")}</td>
                                <td data-label="Pret initial">400</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">360</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services2.subtitle5")}</td>
                                <td data-label="Pret initial">4950</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">4455</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services2.subtitle6")}</td>
                                <td data-label="Pret initial">2300</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">2070</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>








            <div>
                <img
                    alt="Img-tooth"
                    src={tooth}
                    width="50"
                    height="50"
                />
                <br></br>
                <h3>{t("services3")}</h3>

            </div>


            <div className="tarif">
                <div className="row">
                    <div className="column">
                        <img
                            alt="Aparat"
                            src={stomatologieCopii}
                            width="300"
                            height="200"
                        />
                    </div>

                    <div className="column">
                        <p>{t("services3.description")}</p>



                        <table>
                            <thead>
                            <tr>
                                <th scope="col">{t("services.column1")}</th>
                                <th scope="col">{t("services.column2")}</th>
                                <th scope="col">{t("services.column3")}</th>
                                <th scope="col">{t("services.column4")}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td data-label="Manopera">{t("services3.subtitle1")}</td>
                                <td data-label="Pret initial">300</td>
                                <td data-label="Reducere card">10%</td>
                                <td data-label="Pret final">270</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services3.subtitle2")}</td>
                                <td data-label="Pret initial">330</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">297</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services3.subtitle3")}</td>
                                <td data-label="Pret initial">330</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">297</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services3.subtitle4")}</td>
                                <td data-label="Pret initial">300</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">270</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services3.subtitle5")}</td>
                                <td data-label="Pret initial">4950</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">4455</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services3.subtitle6")}</td>
                                <td data-label="Pret initial">350</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">315</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services3.subtitle7")}</td>
                                <td data-label="Pret initial">330</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">297</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>






            <div>
                <img
                    alt="Img-tooth"
                    src={tooth}
                    width="50"
                    height="50"
                />
                <br></br>
                <h3>{t("services4")}</h3>

            </div>


            <div className="tarif">
                <div className="row">
                    <div className="column">
                        <img
                            alt="chirurgie"
                            src={chirurgie}
                            width="300"
                            height="200"
                        />
                    </div>

                    <div className="column">
                        <p>{t("services4.description")}</p>



                        <table>
                            <thead>
                            <tr>
                                <th scope="col">{t("services.column1")}</th>
                                <th scope="col">{t("services.column2")}</th>
                                <th scope="col">{t("services.column3")}</th>
                                <th scope="col">{t("services.column4")}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td data-label="Manopera">{t("services4.subtitle1")}</td>
                                <td data-label="Pret initial">280</td>
                                <td data-label="Reducere card">10%</td>
                                <td data-label="Pret final">252</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services4.subtitle2")}</td>
                                <td data-label="Pret initial">320</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">296</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services4.subtitle3")}</td>
                                <td data-label="Pret initial">330</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">288</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services4.subtitle4")}</td>
                                <td data-label="Pret initial">380</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">342</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services4.subtitle5")}</td>
                                <td data-label="Pret initial">350</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">315</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services4.subtitle6")}</td>
                                <td data-label="Pret initial">140</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">126</td>
                            </tr>
                            <tr>
                                <td data-label="Manopera">{t("services4.subtitle7")}</td>
                                <td data-label="Pret initial">330</td>
                                <td data-label="Reducere">10%</td>
                                <td data-label="Pret final">297</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>

        </div>


    );
}

export default ServiciiTarife;