import React, {Component} from "react";
import axios from "axios";
import {Nav, Navbar} from "react-bootstrap";
import Logo from "./images/dental-logo-8.png";
import {LinkContainer} from "react-router-bootstrap";
import {Button} from "reactstrap";
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend
} from "recharts";


function onLogoutHandler(){
    localStorage.clear();
}

class AppointmentsStatistics extends Component{

    constructor(props) {
        super(props)
        this.state = {
            statisticsData: [],
        }
    }

    componentDidMount() {
        const user = JSON.parse(localStorage.getItem("userData"));
        this.fetchStatisticsData(user.email)

    }

    fetchStatisticsData(email){
        axios.get('http://localhost:8000/api/appointment/getAppointmentsStatistics/' + email)
            .then(response => {
                let appointments = response.data
                let statisticsData = []

                for (let i = 0; i < appointments.length; i++) {
                    statisticsData.push({
                        "day": new Date(appointments[i].date).getDate(),
                        "appointments": appointments[i].appointments,
                    });

                }

                this.setState({
                    statisticsData:statisticsData
                })
                console.log(this.state.statisticsData);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return(
            <div className="Contact">
                <Navbar className="nav-bar-universal" variant="dark">
                    <Navbar.Brand href="#home">
                        <img className="d-inline-block align-top poz"
                             alt="logo"
                             src={Logo}
                             width="90"
                             height="90"
                        />
                    </Navbar.Brand>

                    <Navbar.Brand href="#home" className="logo-txt">
                        <p>
                            <h3 id="txt-1-unv">Dental </h3>
                            <h3 id="txt-2">Clinic</h3>
                        </p>
                    </Navbar.Brand>


                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto poz">
                            <LinkContainer to={"/"}>
                                <Nav.Link id="text-style" >{'Home'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/despre-noi"}>
                                <Nav.Link id="text-style">{'About Us'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/medici"}>
                                <Nav.Link id="text-style">{'Doctors'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/servicii-tarife"}>
                                <Nav.Link id="text-style">{'Services'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/contact"}>
                                <Nav.Link id="text-style">{'Contact'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/profile"}>
                                <Nav.Link id="text-style">{'Profile'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/doctor-appointments"}>
                                <Nav.Link id="text-style">{'Appointments'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/"}>
                                <Button
                                    className="btn btn-primary text-right"
                                    onClick={onLogoutHandler}
                                >
                                    Logout
                                </Button>
                            </LinkContainer>

                        </Nav>

                    </Navbar.Collapse>

                </Navbar>

                <div className="decor-div">
                    <h1>{'Statistics'}</h1>
                </div>

                <br></br>
                <br></br>

                <BarChart
                    width={1500}
                    height={500}
                    data={this.state.statisticsData}
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="day" />
                    <YAxis dataKey="appointments"/>
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="appointments" fill="#8884d8" />
                </BarChart>



                </div>
        );
    }


}

export default AppointmentsStatistics;