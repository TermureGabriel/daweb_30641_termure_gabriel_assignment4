import React, {Component} from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import axios from "axios";
import {Nav, Navbar} from "react-bootstrap";
import Logo from "./images/dental-logo-8.png";
import {LinkContainer} from "react-router-bootstrap";
import {Button} from "reactstrap";
moment.locale("en-GB");

const localizer = momentLocalizer(moment);

function onLogoutHandler(){
    localStorage.clear();
}

class DoctorAppointments extends Component {

    constructor(props) {
        super(props)
        this.state = {
            finalAppointments: [],
        }
    }

    componentDidMount() {
        const user = JSON.parse(localStorage.getItem("userData"));
        this.fetchDoctorAppointments(user.email)

    }

    fetchDoctorAppointments(email){
        axios.get('http://localhost:8000/api/doctor/getDoctorAppointments/' + email)
            .then(response => {

                let appointments = response.data;
                let doctorAppointments = [];


                for (let i = 0; i < appointments.length; i++) {
                    doctorAppointments.push({
                        "title": appointments[i].name,
                        "start": new Date(appointments[i].date),
                        "end": new Date(appointments[i].date)
                    });

                }
                this.setState({
                    finalAppointments:doctorAppointments
                })

            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {

        return (

            <div>

                <Navbar className="nav-bar-universal" variant="dark">
                    <Navbar.Brand href="#home">
                        <img className="d-inline-block align-top poz"
                             alt="logo"
                             src={Logo}
                             width="90"
                             height="90"
                        />
                    </Navbar.Brand>

                    <Navbar.Brand href="#home" className="logo-txt">
                        <p>
                            <h3 id="txt-1-unv">Dental </h3>
                            <h3 id="txt-2">Clinic</h3>
                        </p>
                    </Navbar.Brand>


                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto poz">
                            <LinkContainer to={"/"}>
                                <Nav.Link id="text-style" >{'Home'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/despre-noi"}>
                                <Nav.Link id="text-style">{'About Us'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/medici"}>
                                <Nav.Link id="text-style">{'Doctors'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/servicii-tarife"}>
                                <Nav.Link id="text-style">{'Services'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/contact"}>
                                <Nav.Link id="text-style">{'Contact'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/profile"}>
                                <Nav.Link id="text-style">{'Profile'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/appointments-statistics"}>
                                <Nav.Link id="text-style">{'Statistics'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/"}>
                                <Button
                                    className="btn btn-primary text-right"
                                    onClick={onLogoutHandler}
                                >
                                    Logout
                                </Button>
                            </LinkContainer>

                        </Nav>

                    </Navbar.Collapse>

                </Navbar>


                <br></br>
                <br></br>

                <div style={{ height: 700 }}>
                    <Calendar
                        localizer={localizer}
                        events={this.state.finalAppointments}
                        startAccessor="start"
                        endAccessor="end"
                    />
                </div>
            </div>
        );
    }
}

export default DoctorAppointments;