import {Form, FormControl, Nav, Navbar} from "react-bootstrap";
import Logo from "./images/dental-logo-8.png";
import {LinkContainer} from "react-router-bootstrap";
import './style/Medici.css';
import tooth from "./images/tooth-logo1.png";
import doctor1 from "./images/doctor-1.jpg";
import doctor2 from "./images/doctor-2.jpg";
import doctor3 from "./images/doctor-3.jpg";
import doctor4 from "./images/doctor-4.jpg";
import doctor5 from "./images/doctor-5.jpg";
import doctor6 from "./images/doctor-6.jpg";
import doctor7 from "./images/doctor-7.jpg";
import {useTranslation} from "react-i18next";




function Medici() {
    const {t}= useTranslation();
    return (
        <div className="Medici">

            <Navbar className="nav-bar-universal" variant="dark">
                <Navbar.Brand href="#home">
                    <img className="d-inline-block align-top poz"
                         alt="logo"
                         src={Logo}
                         width="90"
                         height="90"
                    />
                </Navbar.Brand>

                <Navbar.Brand href="#home" className="logo-txt">
                    <p>
                        <h3 id="txt-1-unv">Dental </h3>
                        <h3 id="txt-2">Clinic</h3>
                    </p>
                </Navbar.Brand>


                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto poz">
                        <LinkContainer to={"/"}>
                            <Nav.Link id="text-style" >{t('menu.acasa')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/noutati"}>
                            <Nav.Link id="text-style">{t('menu.noutati')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/despre-noi"}>
                            <Nav.Link id="text-style">{t('menu.despreNoi')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/medici"}>
                            <Nav.Link id="text-style">{t('menu.medici')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/servicii-tarife"}>
                            <Nav.Link id="text-style">{t('menu.servicii')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/contact"}>
                            <Nav.Link id="text-style">{t('menu.contact')}</Nav.Link>
                        </LinkContainer>
                    </Nav>

                </Navbar.Collapse>

            </Navbar>

        <div className="decor-div">
            <h1>{t("doctors.1")}</h1>
        </div>


            <div className="doctor-zone">
                <img
                    alt="Img-tooth"
                    src={tooth}
                    width="50"
                    height="50"
                />

                <h2>{t("doctors.2")}</h2>
                <br></br>
                <br></br>
                <br></br>



                <section>

                    <div className="row">

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src={doctor1}
                                 className="rounded-circle" alt="doctor_img"
                                 width={"200"}
                                 height={"200"}/>

                            <h5>Dr. Bogdan Marian</h5>
                            <h6>{t("specialization.1")}</h6>
                            <p>{t("doctor1.description")}</p>
                        </div>

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src={doctor2}
                                 className="rounded-circle" alt="doctor_img"
                                 width={"200"}
                                 height={"200"}/>

                            <h5>Dr. Vlasin Ana</h5>
                            <h6>{t("specialization.2")}</h6>
                            <p>{t("doctor2.description")}</p>
                        </div>

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src={doctor3}
                                 className="rounded-circle" alt="doctor_img"
                                 width={"200"}
                                 height={"200"}/>

                            <h5>Dr. Mihnea Albert</h5>
                            <h6>{t("specialization.3")}</h6>
                            <p>{t("doctor3.description")}</p>
                        </div>

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src={doctor4}
                                 className="rounded-circle" alt="doctor_img"
                            width={"200"}
                            height={"200"}/>

                            <h5>Dr. Zagrean Claudia</h5>
                            <h6>{t("specialization.4")}</h6>
                            <p>{t("doctor4.description")}</p>
                        </div>

                    </div>




                    <div className="row">

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src={doctor5}
                                 className="rounded-circle" alt="doctor_img"
                                 width={"200"}
                                 height={"200"}/>

                            <h5>Dr. Damaris Iulia</h5>
                            <h6>{t("specialization.5")}</h6>
                            <p>{t("doctor5.description")}</p>
                        </div>

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src={doctor6}
                                 className="rounded-circle" alt="doctor_img"
                                 width={"200"}
                                 height={"200"}/>

                            <h5>Dr. Muresan Vlad</h5>
                            <h6>{t("specialization.6")}</h6>
                            <p>{t("doctor6.description")}</p>
                        </div>

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src={doctor7}
                                 className="rounded-circle" alt="doctor_img"
                                 width={"200"}
                                 height={"200"}/>

                            <h5>Dr. Moldovan Diana</h5>
                            <h6>{t("specialization.7")}</h6>
                            <p>{t("doctor7.description")}</p>
                        </div>

                        <div className="col-lg-3 col-md-6 mb-r">

                            <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(1).jpg"
                                 className="rounded-circle" alt="doctor_img"
                                 width={"200"}
                                 height={"200"}/>

                            <h5>Dr. Condrea Sara</h5>
                            <h6>{t("specialization.8")}</h6>
                            <p>{t("doctor8.description")}</p>
                        </div>

                    </div>

                </section>


            </div>



        </div>
    );
}

export default Medici;