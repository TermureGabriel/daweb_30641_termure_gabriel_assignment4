import {Nav, Navbar} from "react-bootstrap";
import Logo from "./images/dental-logo-8.png";
import {LinkContainer} from "react-router-bootstrap";
import tooth from "./images/tooth-logo1.png";
import "./style/DespreNoi.css";
import cabinet from "./images/cabinet.jpg";
import cabinet2 from "./images/cabinet-2.jpg";
import cabinet3 from "./images/tehnologie.jpg";
import {useTranslation} from "react-i18next";

function DespreNoi(){
    const {t}= useTranslation();
    return(
        <div className="App">
            <Navbar className="nav-bar-universal" variant="dark">
                <Navbar.Brand href="#home">
                    <img className="d-inline-block align-top poz"
                         alt="logo"
                         src={Logo}
                         width="90"
                         height="90"
                    />
                </Navbar.Brand>

                <Navbar.Brand href="#home" className="logo-txt">
                    <p>
                        <h3 id="txt-1-unv">Dental </h3>
                        <h3 id="txt-2">Clinic</h3>
                    </p>
                </Navbar.Brand>


                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto poz">
                        <LinkContainer to={"/"}>
                            <Nav.Link id="text-style" >{t('menu.acasa')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/noutati"}>
                            <Nav.Link id="text-style">{t('menu.noutati')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/despre-noi"}>
                            <Nav.Link id="text-style">{t('menu.despreNoi')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/medici"}>
                            <Nav.Link id="text-style">{t('menu.medici')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/servicii-tarife"}>
                            <Nav.Link id="text-style">{t('menu.servicii')}</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to={"/contact"}>
                            <Nav.Link id="text-style">{t('menu.contact')}</Nav.Link>
                        </LinkContainer>
                    </Nav>

                </Navbar.Collapse>

            </Navbar>

            <div className="decor-div">
                <h1>{t("about.1")}</h1>
            </div>

            <div className="doctor-zone">
                <img
                    alt="Img-tooth"
                    src={tooth}
                    width="50"
                    height="50"
                />

                <h2>{t("about.2")}</h2>
                <br></br>
                <br></br>
                <br></br>
            </div>

            <div className="main-grid">

                <img className="main-image"
                     src={cabinet2}
                     alt="cabinet"/>

                <img className="main-image"
                     src={cabinet}
                     alt="cabinet"/>

                <img className="main-image"
                     src={cabinet3}
                     alt="cabinet"/>

                    <div className="main-text">
                        <h2 className="section-title">{t("about.title1")}</h2>
                        <p>{t("about.description1")}</p>
                        <p>{t("about.description2")}</p>

                        <h2 className="section-title sub">{t("about.title2")}</h2>
                        <p>{t("about.description3")}</p>
                        <p>{t("about.description4")}</p>
                    </div>




            </div>


        </div>
    );
}

export default DespreNoi;