import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';
import App from './App';
import Medici from './Medici';
import DespreNoi from './DespreNoi';
import Noutati from './Noutati';
import Contact from './Contact';
import ServiciiTarife from "./ServiciiTarife";
import reportWebVitals from './reportWebVitals';
import DoctorAppointments from "./DoctorAppointments";
import AppointmentsStatistics from "./AppointmentsStatistics";
import Login from './Login';
import Profile from './Profile';
import Appointment from "./Appointment";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import './i18n';
const Routing=()=>{

    return(
        <React.Fragment>
            <Router>
                <Switch>
                    <Route exact path={"/"} component = {App} />
                    <Route exact path={"/medici"} component={Medici} />
                    <Route exact path={"/despre-noi"} component={DespreNoi} />
                    <Route exact path={"/contact"} component={Contact} />
                    <Route exact path={"/noutati"} component={Noutati} />
                    <Route exact path={"/servicii-tarife"} component={ServiciiTarife} />
                    <Route exact path={"/login"} component={Login}/>
                    <Route exact path={"/profile"} component={Profile} />
                    <Route exact path={"/appointment"} component={Appointment} />
                    <Route exact path={"/doctor-appointments"} component={DoctorAppointments} />
                    <Route exact path={"/appointments-statistics"} component={AppointmentsStatistics}/>
                </Switch>
            </Router>
        </React.Fragment>
    )
}

ReactDOM.render(
  <React.StrictMode>

      <Routing />

  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
