
import {Navbar, NavItem, NavDropdown, MenuItem, Nav, Form, FormControl, Button, Image} from 'react-bootstrap';
import {Card, CardDeck } from "react-bootstrap";
import './style/App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import Logo from "./images/dental-logo-8.png"
import img1 from "./images/dental-3.jpg";
import tooth from "./images/tooth-logo1.png";
import implant from "./images/implant.png";
import estetica from "./images/estetica-dentara.jpg";
import ortodentie from "./images/ortodentie.jpg";
import {LinkContainer} from "react-router-bootstrap";
import video from "./images/prezentare-clinica.mp4";
import React from 'react';
import SelectLanguage from './select-component';
import {useTranslation} from "react-i18next";

function onLogoutHandler(){
    localStorage.clear();
}

function App(){
    const {t}= useTranslation();

    const user = JSON.parse(localStorage.getItem("userData"));
  return (
    <div className="App">
      <header>
          <title>Dental-Clinic</title>
      </header>

        <Navbar className="nav-bar" variant="dark">
            <Navbar.Brand href="/">
                <img className="d-inline-block align-top poz"
                    alt="logo"
                    src={Logo}
                    width="100"
                    height="100"
                />
            </Navbar.Brand>

            <Navbar.Brand href="#home" className="logo-txt">
                <p>
                    <h3 id="txt-1">Dental </h3>
                    <h3 id="txt-2">Clinic</h3>
                </p>
            </Navbar.Brand>
        </Navbar>

        <Navbar className="nav-bar-2">
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto poz">
                    <LinkContainer to={"/"}>
                        <Nav.Link id="text-style" >{t('menu.acasa')}</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to={"/noutati"}>
                        <Nav.Link id="text-style">{t('menu.noutati')}</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to={"/despre-noi"}>
                        <Nav.Link id="text-style">{t('menu.despreNoi')}</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to={"/medici"}>
                        <Nav.Link id="text-style">{t('menu.medici')}</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to={"/servicii-tarife"}>
                        <Nav.Link id="text-style">{t('menu.servicii')}</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to={"/contact"}>
                        <Nav.Link id="text-style">{t('menu.contact')}</Nav.Link>
                    </LinkContainer>

                    <SelectLanguage/>

                    <LinkContainer to={"/login"}>
                        <Nav.Link id="text-style"> Sign In </Nav.Link>
                    </LinkContainer>
                    <Button
                        className="btn btn-primary text-right"
                        onClick={onLogoutHandler}
                    >
                        Logout
                    </Button>

                    <LinkContainer to={"/appointment"}>
                        <Nav.Link id="text-style">{'Appointment'}</Nav.Link>
                    </LinkContainer>
                </Nav>

        </Navbar.Collapse>
        </Navbar>

        <img id="img-1"
             alt="Img"
             src={img1}
        />




        <div className="card-zone">

            <img
                 alt="Img-tooth"
                 src={tooth}
                 width="80"
                 height="80"
            />

            <h2>{t('home.1')}</h2>
            <br></br>
            <br></br>
            <br></br>
            <br></br>

            <CardDeck>
                <Card className="card-content">
                    <Card.Img variant="top" src={implant} />
                    <Card.Body>
                        <Card.Title>{t("home.title.card.1")}</Card.Title>
                        <Card.Text>
                            {t("home.description.card1")}
                        </Card.Text>
                    </Card.Body>
                </Card>
                <Card className="card-content">
                    <Card.Img variant="top" src={estetica} height="200"/>
                    <Card.Body>
                        <Card.Title>{t("home.title.card.2")}</Card.Title>
                        <Card.Text>
                            {t("home.description.card2")}
                        </Card.Text>
                    </Card.Body>
                </Card>
                <Card className="card-content">
                    <Card.Img variant="top" src={ortodentie} height="200"/>
                    <Card.Body>
                        <Card.Title>{t("home.title.card.3")}</Card.Title>
                        <Card.Text>
                            {t("home.description.card3")}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </CardDeck>

        </div>

        <br></br>
        <br></br>
        <br></br>

        <img
            alt="Img-tooth"
            src={tooth}
            width="80"
            height="80"
        />

        <h2>{t("home.2")}</h2>
        <br></br>
        <br></br>
        <br></br>



        <video width="80%" height="80%" controls>
            <source src={video}/>
        </video>

        <br></br>
        <br></br>
        <br></br>





    </div>

  );
}

export default App;
