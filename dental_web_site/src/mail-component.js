import React from "react";
import {Button, Modal, ModalBody, Input} from 'reactstrap';

export default class MailComponent extends React.Component{

    constructor(props){

        super(props);
        this.state = {
            count: props.count,
            modal: false
        };

        this.toggle = this.toggle.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    render(){

        return(
            <div>
                <Button onClick={this.toggle}>Open</Button>

                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalBody>
                        <Input type="textarea" placeholder="Write something" rows={5} />
                    </ModalBody>
                </Modal>
            </div>

        );



    }

    toggle(){
        this.setState({
            modal: !this.state.modal
        });


    }

    handleClick(){

        this.setState({
            count: this.state.count + 1
        });

    }











}