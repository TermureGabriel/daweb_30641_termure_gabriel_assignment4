import "./style/Profile.css"
import React, { Component }from "react";
import axios from "axios";
import {Button, Nav, Navbar} from "react-bootstrap";
import Logo from "./images/dental-logo-8.png";
import {LinkContainer} from "react-router-bootstrap";
import {useTranslation} from "react-i18next";

function onLogoutHandler(){
    localStorage.clear();
}

export default class Profile extends Component{
    constructor(props) {
        super(props);
        this.state = {
            id:"",
            name:"",
            email: "",
            password: "",
            servicii: "",
        };
    }

    componentDidMount() {
        const user = JSON.parse(localStorage.getItem("userData"));
        this.findUserById(user.id);
    }

    findUserById(id){
        axios.get("http://localhost:8000/api/user/findById/" + id)
            .then((response) => {
                    this.setState({
                        id: response.data.id,
                        name: response.data.name,
                        email: response.data.email,
                        password: response.data.password,
                        servicii: response.data.servicii,
                    });

            })
            .catch((error) => {
                console.log(error);
            });
    }

    onChangehandler = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        let data = {};
        data[name] = value;
        this.setState(data);
    };

    setServices = (e) => {

        this.setState({servicii: this.state.servicii + e.target.value});
    }

    updateUser = () =>{
        axios
            .put("http://localhost:8000/api/user/update", {
                id:this.state.id,
                name:this.state.name,
                email: this.state.email,
                password: this.state.password,
                servicii: this.state.servicii,
            })
            .then((response) => {
                console.log(response.data);
            })

    }

    render() {
        return(
            <div>
                <Navbar className="nav-bar-universal" variant="dark">
                    <Navbar.Brand href="#home">
                        <img className="d-inline-block align-top poz"
                             alt="logo"
                             src={Logo}
                             width="90"
                             height="90"
                        />
                    </Navbar.Brand>

                    <Navbar.Brand href="#home" className="logo-txt">
                        <p>
                            <h3 id="txt-1-unv">Dental </h3>
                            <h3 id="txt-2">Clinic</h3>
                        </p>
                    </Navbar.Brand>


                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto poz">
                            <LinkContainer to={"/"}>
                                <Nav.Link id="text-style" >{'Home'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/noutati"}>
                                <Nav.Link id="text-style">{'News'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/despre-noi"}>
                                <Nav.Link id="text-style">{'About Us'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/medici"}>
                                <Nav.Link id="text-style">{'Doctors'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/servicii-tarife"}>
                                <Nav.Link id="text-style">{'Services'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/contact"}>
                                <Nav.Link id="text-style">{'Contact'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/appointment"}>
                                <Nav.Link id="text-style">{'Appointment'}</Nav.Link>
                            </LinkContainer>


                        </Nav>

                    </Navbar.Collapse>

                </Navbar>

                <div className="decor-div">
                    <h1>{'Personal Info'}</h1>
                </div>
                <br></br>
                <br></br>
                <div>
                    <section className="module">
                        <div>

                                    <div>
                                        <figure className="figure">
                                            <img className="img-rounded img-responsive"
                                                 src="https://bootdey.com/img/Content/avatar/avatar1.png" alt=""/>
                                        </figure>
                                        <div className="form-inline">
                                            <input type="file" className="file-uploader pull-left"/>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label">User Name</label>
                                        <div>
                                            <input type="text" className="form-control"
                                                   name="name"
                                                   value={this.state.name}
                                                   onChange={this.onChangehandler}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label">Email</label>
                                        <div>
                                            <input type="text" className="form-control"
                                                   name="email"
                                                   value={this.state.email}
                                                   onChange={this.onChangehandler}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Password</label>
                                        <div>
                                            <input type="text" className="form-control"
                                                   name="password"
                                                   value={this.state.password}
                                                   onChange={this.onChangehandler}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label">Services</label>
                                        <div>
                                            <input type="text" className="form-control"
                                                   name="servicii"
                                                   value={this.state.servicii}
                                                   onChange={this.onChangehandler}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <select onChange={this.setServices}>
                                            <option value={"implant dentar "}>Implant Dentar</option>
                                            <option value={"aparat dentar "}>Aparat dentar</option>
                                            <option value={"stomatologie copii "}>Stomatologie copii</option>
                                            <option value={"chirurgie orala "}>Chirurgie orala</option>
                                        </select>
                                    </div>



                                    <div className="form-group">
                                        <div>
                                            <input className="btn btn-primary" type="submit" value="Update Profile"
                                                    onClick={this.updateUser}
                                            />
                                        </div>
                                    </div>


                            </div>
                    </section>
                </div>
            </div>
        );
    }


}
