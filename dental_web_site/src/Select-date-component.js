import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
} from "reactstrap";

export default class selectDate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDate: new Date(),
        }
    }

    onChangeEvent = (event) => {

        this.setState({ selectedDate: event });

        this.props.appointmentModalData.date = event;
        console.log(event);
    };

    render() {

        return (

            <div>
                <Modal
                    isOpen={this.props.selectDateModal}
                    toggle={this.props.toggleSelectDateModal}
                >
                    <ModalHeader toggle={this.props.toggleSelectDateModal}>
                        Select Date
                    </ModalHeader>
                    <ModalBody>

                        <DatePicker showTimeSelect
                                    timeFormat="HH:mm"
                                    timeIntervals={30}
                                    timeCaption="time"
                                    name="date"
                                    id="date"
                                    dateFormat={"MMMM d, yyyy h:mm aa"}
                                    selected={this.state.selectedDate}
                                    onChange={this.onChangeEvent}
                        />


                    </ModalBody>
                    <ModalFooter>
                        <Button
                            color="primary"
                            onClick={this.props.makeAppointment}
                        >
                            Make Appointment
                        </Button>
                        <Button
                            color="secondary"
                            onClick={this.props.toggleSelectDateModal}
                        >
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}