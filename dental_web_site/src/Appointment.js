import React, { Component } from "react";
import { Table, Button } from "reactstrap";
import axios from "axios";
import SelectDateModal from "./Select-date-component";
import {Nav, Navbar} from "react-bootstrap";
import Logo from "./images/dental-logo-8.png";
import {LinkContainer} from "react-router-bootstrap";
import './style/Appointment.css';

function onLogoutHandler(){
    localStorage.clear();
}

export default class Appointment extends Component {
    constructor(props){
        super(props);
        this.state = {
            doctors: [],
            noDataFound: "",
            selectDateModal: false,
            appointmentModalData: {
                doctor_id: "",
                user_id: "",
                date: "",
            },
        }
    }

    componentDidMount() {
        this.getDoctors();
    }

    getDoctors() {
        axios.get("http://localhost:8000/api/doctor/getAllDoctors").then((response) => {
            if (response.data.status === "success" &&
                response.data.success === true) {
                this.setState({
                    doctors: response.data.data ? response.data.data : [],
                });
            }
            if (
                response.data.status === "failed" &&
                response.data.success === false
            ) {
                this.setState({
                    noDataFound: response.data.message,
                });
            }
        });
    }

    selectDate(doctor_id, user_id){
        this.setState({
            appointmentModalData: {doctor_id, user_id},
            selectDateModal: !this.state.selectDateModal,
        });
    }

    toggleSelectDateModal = () => {
        this.setState({
            selectDateModal: !this.state.selectDateModal,
        });
    }

    onChangeSelectDateHandler = (e) => {
        let { appointmentModalData } = this.state;
        appointmentModalData[e.target.name] = e.target.value;
        this.setState({ appointmentModalData });
    };

    makeAppointment = () => {
        let {
            doctor_id,
            user_id,
            date,
        } = this.state.appointmentModalData;

        this.setState({
            isLoading: true,
        });
        axios
            .post("http://localhost:8000/api/appointment/add", {
                user_id,
                doctor_id,
                date,
            })

        this.setState({
            selectDateModal: !this.state.selectDateModal,
        });
    };

    render() {
        const user = JSON.parse(localStorage.getItem("userData"));
        const { noDataFound, doctors} = this.state;
        let doctorsDetails = [];
        if (doctors.length) {
            doctorsDetails = doctors.map((doctor) => {
                return (
                    <tr key={doctor.id}>
                        <td>{doctor.id}</td>
                        <td>{doctor.name}</td>
                        <td>{doctor.email}</td>
                        <td>{doctor.specialization}</td>
                        <td>
                            <Button
                                color="success"
                                size="sm"
                                onClick={() =>
                                    this.selectDate(
                                        doctor.id,
                                        user.id
                                    )
                                }
                            >
                                Select Date
                            </Button>
                        </td>
                    </tr>
                );
            });
        }

        return (
            <div className={'Appointment'}>

                <Navbar className="nav-bar-universal" variant="dark">
                    <Navbar.Brand href="#home">
                        <img className="d-inline-block align-top poz"
                             alt="logo"
                             src={Logo}
                             width="90"
                             height="90"
                        />
                    </Navbar.Brand>

                    <Navbar.Brand href="#home" className="logo-txt">
                        <p>
                            <h3 id="txt-1-unv">Dental </h3>
                            <h3 id="txt-2">Clinic</h3>
                        </p>
                    </Navbar.Brand>


                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto poz">
                            <LinkContainer to={"/"}>
                                <Nav.Link id="text-style" >{'Home'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/noutati"}>
                                <Nav.Link id="text-style">{'News'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/despre-noi"}>
                                <Nav.Link id="text-style">{'About Us'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/medici"}>
                                <Nav.Link id="text-style">{'Doctors'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/servicii-tarife"}>
                                <Nav.Link id="text-style">{'Services'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/contact"}>
                                <Nav.Link id="text-style">{'Contact'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/profile"}>
                                <Nav.Link id="text-style">{'Profile'}</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to={"/"}>
                                <Button
                                    className="btn btn-primary text-right"
                                    onClick={onLogoutHandler}
                                >
                                    Logout
                                </Button>
                            </LinkContainer>

                        </Nav>

                    </Navbar.Collapse>

                </Navbar>

                <div className="decor-div">
                    <h1>{'Make appointment'}</h1>
                </div>
                <br></br>
                <br></br>
                <SelectDateModal
                    toggleSelectDateModal={this.toggleSelectDateModal}
                    selectDateModal={this.state.selectDateModal}
                    onChangeSelectDateHandler={this.onChangeSelectDateHandler}
                    selectDate={this.selectDate}
                    appointmentModalData={this.state.appointmentModalData}
                    makeAppointment={this.makeAppointment}
                />
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Specialization</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    {doctors.length === 0 ? (
                        <tbody>
                        <h3>{noDataFound}</h3>
                        </tbody>
                    ) : (
                        <tbody>{doctorsDetails}</tbody>
                    )}
                </Table>
            </div>
        );
    }
}